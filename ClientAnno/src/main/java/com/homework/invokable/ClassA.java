package com.homework.invokable;

import com.homework.client.EndOfCommunication;

public class ClassA {

	
	@InvokingAnnotation(className="com.homework.invokable.ClassB",methodName="methodE")
	public void methodA()
	{
		System.out.println("methodA invoked");
	}
	
	@InvokingAnnotation(className="com.homework.invokable.ClassB",methodName="methodF")
	public void methodB()
	{
		System.out.println("methodB invoked");

	}
	
	public void methodC() throws EndOfCommunication
	{
		System.out.println("methodC invoked");
		throw new EndOfCommunication();
	}
}

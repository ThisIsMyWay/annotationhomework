package com.homework.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.homework.invokable.InvokingAnnotation;

public class MessageExchanger {
	BufferedReader reader;
	BufferedWriter writer;
	public MessageExchanger(BufferedReader reader, BufferedWriter writer) throws IOException
	{
		this.reader =reader;
		this.writer = writer;
		startCommunication();
		
	}
	
	private void startCommunication() throws IOException
	{		
		sendInitialMessage();
		while (true)
		{
			try 
			{
				tryToInvokeAppropriateMethodBasedOnMessage();		
			}
			catch(EndOfCommunication ex)
			{
				writeMessageToSocketAndSend("end");
				break;
			}
		}
	}
	
	private void sendInitialMessage() throws IOException
	{
			writeMessageToSocketAndSend("com.homework.invokable.ClassB;methodD");
	}

	private  void tryToInvokeAppropriateMethodBasedOnMessage() throws EndOfCommunication
	{
		try {
			String message = reader.readLine();
		
			String[] listOfClassAndMethodStringId = getClassAndMethodStringNamesFromMessage(message);
			System.out.println(message);
		    writeMessageToSocketAndSend(getInfoToSendAndMakeActionBasedOnMessage(listOfClassAndMethodStringId));
						
		}
		catch (EndOfCommunication ex) 
		{
			System.out.println(ex.toString());
			
		}
		catch (Exception e) 
		{			
			if (e.getCause() instanceof EndOfCommunication)
			{
				throw new EndOfCommunication();
			}			
		}		
	}
	


	

	private <T>  String getInfoToSendAndMakeActionBasedOnMessage(String[] listOfClassAndMethodStringId) throws Exception {
	
		T retrievedObject = getObjectByName(listOfClassAndMethodStringId[0]);
		Method methodToInvoke = findMethodOfObjectByName(retrievedObject, listOfClassAndMethodStringId[1]);
		if (methodToInvoke == null)
		{
			throw new CantFindMethodException();
		}
	    methodToInvoke.invoke(retrievedObject, (Object[])null);
		return getMessageToSendFromMethodAnnotation(methodToInvoke);
	}

	private <T> T getObjectByName(String name) throws ClassNotFoundException, IllegalAccessException, InstantiationException
	{
		Class<?> cls = Class.forName(name);
		
		Object obj = cls.newInstance();
	
		return (T) cls.cast(obj);
	}
	
	
	private <T> Method findMethodOfObjectByName(T methodToSearch, String nameOfMethod)
	{
		for (Method m : methodToSearch.getClass().getMethods())
		{
			System.out.println(m.getName());
			 if (nameOfMethod.equals(m.getName()))
			 {					 
				 return m;
			 }
		}
		return null;
	}


	private String getMessageToSendFromMethodAnnotation(Method method)
	{
		InvokingAnnotation invokingAnnotation = method.getAnnotation(InvokingAnnotation.class);
		return invokingAnnotation.className()+";"+invokingAnnotation.methodName();
	}
	
	
	private String[] getClassAndMethodStringNamesFromMessage(String message) {
		return message.split(";");
	}
	
	private void writeMessageToSocketAndSend(String contentToSend) throws IOException
	{
		writer.write(contentToSend);
		writer.newLine();
		writer.flush();
	}
	
}

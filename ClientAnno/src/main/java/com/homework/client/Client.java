package com.homework.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;


public class Client {
	   private InetSocketAddress address;

	
	    public Client(InetSocketAddress address) {
	        this.address = address;
	        try(
	            Socket socket = new Socket(InetAddress.getByName(address.getHostName()), this.address.getPort());
	            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
	        ) 
	        {
		        new MessageExchanger(reader, writer);  	     
		        System.out.println("End communication");
	        } 	      
	        catch (Exception e) {
	            e.printStackTrace();
	        }
	    }

	    public static void main(String[] args) throws Exception {
	        new Client(new InetSocketAddress(InetAddress.getLocalHost(), 9191));
	    }
}

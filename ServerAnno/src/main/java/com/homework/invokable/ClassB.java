package com.homework.invokable;

public class ClassB {

	
	@InvokingAnnotation(className="com.homework.invokable.ClassA",methodName="methodA")
	public void methodD()
	{
		System.out.println("methodD invoked");
	}
	
	@InvokingAnnotation(className="com.homework.invokable.ClassA",methodName="methodB")
	public void methodE()
	{
		System.out.println("methodE invoked");

	}
	
	@InvokingAnnotation(className="com.homework.invokable.ClassA",methodName="methodC")
	public void methodF()
	{
		System.out.println("methodF invoked");

	}
}

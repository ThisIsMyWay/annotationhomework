package com.homework.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Time;


public class Server {

	public Server() {
		super();
		try (ServerSocket server = new ServerSocket(9191);
				Socket clientSocket = server.accept();
				BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));) {
			
			System.out.println("Server");
	        new MessageExchangerServer(reader, writer);  	 
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	 public static void main(String[] args) {
		 System.out.println("SErver is running");
	        new Server();
	    }
}

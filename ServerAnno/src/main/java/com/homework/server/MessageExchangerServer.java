package com.homework.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.homework.invokable.InvokingAnnotation;
import com.sun.org.apache.xerces.internal.xs.StringList;

import jdk.nashorn.internal.runtime.FindProperty;
import sun.font.TrueTypeFont;

public class MessageExchangerServer {
	BufferedReader reader;
	BufferedWriter writer;
	public MessageExchangerServer(BufferedReader reader, BufferedWriter writer) throws IOException
	{
		this.reader =reader;
		this.writer = writer;
		startCommunication();
		
	}
	
	private void startCommunication() throws IOException
	{		
//		sendInitialMessage();
		while (true)
		{
			try 
			{
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				tryToInvokeAppropriateMethodBasedOnMessage();		
			}
			catch(EndOfCommunication ex)
			{
				break;
			}
		}
	}
	
	private  void tryToInvokeAppropriateMethodBasedOnMessage() throws EndOfCommunication
	{
		try {
			String message = reader.readLine();
			checkIfClientClosedConnection(message);		
			String[] listOfClassAndMethodStringId = getClassAndMethodStringNamesFromMessage(message);
		    writeMessageToSocketAndSend(getInfoToSendAndMakeActionBasedOnMessage(listOfClassAndMethodStringId));
						
		} 
		catch (EndOfCommunication ex)
		{
			throw new EndOfCommunication();
		}
		catch (Exception e) {
			e.printStackTrace();			
			System.out.println(e.toString());
			
		}
		
	}
	
	private void checkIfClientClosedConnection(String message) throws EndOfCommunication
	{
		if (message.equals("end"))
		{
			throw new EndOfCommunication();
		}
	}

	

	private String[] getClassAndMethodStringNamesFromMessage(String message) {
		return message.split(";");
	}
	
	private <T>  String getInfoToSendAndMakeActionBasedOnMessage(String[] listOfClassAndMethodStringId) throws ClassNotFoundException, IllegalAccessException, InstantiationException, CantFindMethodException, IllegalArgumentException, InvocationTargetException, EndOfCommunication{
	
		T retrievedObject = getObjectByName(listOfClassAndMethodStringId[0]);
		Method methodToInvoke = findMethodOfObjectByName(retrievedObject, listOfClassAndMethodStringId[1]);
		if (methodToInvoke == null)
		{
			throw new CantFindMethodException();
		}
	    methodToInvoke.invoke(retrievedObject, (Object[])null);
		return getMessageToSendFromMethodAnnotation(methodToInvoke);
	}

	private <T> T getObjectByName(String name) throws ClassNotFoundException, IllegalAccessException, InstantiationException
	{
		Class<?> cls = Class.forName(name);
		
		Object obj = cls.newInstance();
	
		return (T) cls.cast(obj);
	}
	
	
	private <T> Method findMethodOfObjectByName(T methodToSearch, String nameOfMethod)
	{
		for (Method m : methodToSearch.getClass().getMethods())
		{
			System.out.println(m.getName());
			 if (nameOfMethod.equals(m.getName()))
			 {					 
				 return m;
			 }
		}
		return null;
	}


	private String getMessageToSendFromMethodAnnotation(Method method)
	{
		InvokingAnnotation invokingAnnotation = method.getAnnotation(InvokingAnnotation.class);
		return invokingAnnotation.className()+";"+invokingAnnotation.methodName();
	}
	
	
	
	private void writeMessageToSocketAndSend(String contentToSend) throws IOException
	{
		writer.write(contentToSend);
		writer.newLine();
		writer.flush();
	}
	
}
